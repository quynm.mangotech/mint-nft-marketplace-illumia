// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

contract MyNFT is ERC721URIStorage, Ownable {
    address payable ownerContract;

    constructor() ERC721("QUYCUTEEEE", "QUY") {
        ownerContract = payable(msg.sender);
    }

    function transferToken(uint256 _amount, uint256 _price) external payable {
        require(msg.value == _price * _amount, "You must pay the price of the item");
        ownerContract.transfer(msg.value);
    }

    function mintNFT(address recipient, string memory tokenURI, uint256 tokenId)
        external onlyOwner
        returns (uint256)
    {
        _mint(recipient, tokenId);
        _setTokenURI(tokenId, tokenURI);
        return tokenId;
    }
}
