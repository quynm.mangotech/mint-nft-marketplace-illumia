require("@nomicfoundation/hardhat-toolbox");
require('dotenv').config()

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.9",
  networks: {
    rinkeby: { 
      url: process.env.RINKEBY_API_KEY,
      accounts: [process.env.PRIVATE_KEY]
      // 0x1321F771bb0789Fb4c346b9214dcC3D4F59991DA
      // 0x2c33CF2b357a09D93DFD1B42121Fc76141443767
    },
    goerli: {
      url: process.env.GOERLI_API_KEY,
      accounts: [process.env.PRIVATE_KEY]
      //0x8D6Cda257B2b767ab87dE8F24b21C70188E7c525
    },
    mumbai: {
      url: process.env.MUMBAI_API_KEY,
      accounts: [process.env.PRIVATE_KEY]
      // 0xDac1EFa3B9Fe84BB0cC13cD35f4213376496809e
    },
    ropsten: {
      url: process.env.ROPSTEN_API_KEY,
      accounts: [process.env.PRIVATE_KEY]
      // 0xDac1EFa3B9Fe84BB0cC13cD35f4213376496809e
      // 0xBD7E4c50D8DA361bBF3Ee483bd5D30e1676cEAa2
    },
    kovan: {
      url: process.env.KOVAN_API_KEY,
      accounts: [process.env.PRIVATE_KEY]
      //0xBD7E4c50D8DA361bBF3Ee483bd5D30e1676cEAa2
    },
    bsc: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545",
      chainId: 97,
      gasPrice: 20000000000,
      accounts: [process.env.PRIVATE_KEY],
      // 0x2d57B2A08C998A04F12C58Be07F94B1dAa3Cf41D
    },
    baobab: {
      url: "https://api.baobab.klaytn.net:8651",
      chainId: 1001,
      accounts: [process.env.PRIVATE_KEY],
      // 0xDac1EFa3B9Fe84BB0cC13cD35f4213376496809e
    }
  }
  
};
